package com.gitlab.dudex

import com.gitlab.dudex.logging.Logging
import io.vertx.core.Verticle
import io.vertx.core.Vertx
import io.vertx.kotlin.core.deployVerticleAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlin.reflect.KClass

fun main() {
    System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory")
    AppStarter().launch()
}

class AppStarter : Logging {

    private val vertx  = Vertx.vertx()

    fun launch() {
        try {
            log.info { "Start verticle deployment ..." }
            // Insert technology verticle here
        } catch (ex: Throwable) {
            log.error("CRITICAL! App could not be started. Shutdown application...", ex)
            vertx.close()
        }
    }

    private suspend fun deploy(clazz: KClass<out Verticle>) {
        val name = clazz.simpleName!!
        log.info { "Start verticle $name..." }
        vertx.deployVerticleAwait(clazz.java.canonicalName)
        log.info { "Verticle $name successful started." }
    }

}