package com.gitlab.dudex.logging

import mu.KLogger
import mu.KotlinLogging

interface Logging {
    val log: KLogger
        get() = KotlinLogging.logger(this::class.qualifiedName!!)
}
